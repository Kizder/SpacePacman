﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GereCollision : MonoBehaviour
{
	int scoreeaten = 0;
	public int livePlayer;
	public int score;
	public float CanIEat = 0;
	public Text TextScore;
	public Text TextLive;
	public Text TextMessageFinal;
	public Text TextScoreFinal;
	public Text TextBonus;
	public Text TextDead;
	public AudioClip sound;
	public GameObject guiPlayer;
	public GameObject menufinal;
	public GameObject cameraEnabled;
	public GameObject botGenerator;
	public GameObject menuInGame;
	public GameObject triggerSound;
	public Light lightPlayer;

	/*
	** Réduis la durée du bonus et écris les informations et check pour le menu en jeu
	*/
	void Update ()
	{
		GereMenuInGame ();
		if (CanIEat >= 0)
		{
			CanIEat -= Time.deltaTime;
		}
		else
		{
			TextBonus.text = "Bonus : None";
			lightPlayer.light.range = 5;
			lightPlayer.light.spotAngle = 80;
		}
		TextScore.text = "Score : " + score.ToString();
		TextLive.text = "Lives : x" + livePlayer.ToString ();
	}

	/*
	** Check les triggers des Score, des Enemy et des Bonus.
	** Fais la gestion des bonus
	*/
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Score")
		{
			TouchByScore(other);
		}
		else if (other.gameObject.tag == "Enemy")
		{
			MeetEnemy(other);
		}
		else if (other.gameObject.tag == "Bonus")
		{
			lightPlayer.light.range = 100;
			lightPlayer.light.spotAngle = 120;
			TextBonus.text = "Bonus : Active";
			Destroy(other.gameObject);
			CanIEat = 10;
		}
	}

	/*
	** Check les conditions de victoires (nombres de scores manger)
	*/
	void GereVictoire()
	{
		if (scoreeaten == 282)
		{
			menufinal.SetActive(true);
			TextMessageFinal.text = "Victory";
			TextScoreFinal.text = "Score : " + score.ToString ();
		}
	}

	/*
	** Ouvre le menu en jeu ou le ferme
	*/
	void GereMenuInGame()
	{
		if (Input.GetKeyDown (KeyCode.Escape) && Time.timeScale == 1)
		{
			menuInGame.SetActive(true);
			Time.timeScale = 0;
		}
		else if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale == 0)
		{
			menuInGame.SetActive(false);
			Time.timeScale = 1;
		}
	}

	/*
	** S'occupe de la mort du joueur. Désactive le joueur, désactive l'interface en jeu du joueur.
	** Active la caméra de fin de jeu et le menu final.
	*/
	void DeadPlayer()
	{
		cameraEnabled.SetActive(true);
		gameObject.SetActive(false);
		guiPlayer.SetActive(false);
		menufinal.SetActive (true);
		TextMessageFinal.text = "Game Over";
		TextScoreFinal.text = "Score : " + score.ToString ();
	}

	/*
	** Détruit le score, joue le son sound, augmente le score et incremente la condition de victoire
	*/
	void TouchByScore(Collider other)
	{
		Destroy (other.gameObject);
		audio.PlayOneShot (sound);
		score = score + 10;
		scoreeaten = scoreeaten + 1;
	}

	/*
	** Gére les rencontres avec un bot.
	** Si pas de bonus : mort du joueur ou téléportation et réinitialisation des robots
	** Sinon augmente le nombre de points
	*/
	void MeetEnemy(Collider other)
	{
		Vector3 newposition = new Vector3 (0f, 1f, -10.5f);

		if (CanIEat <= 0)
		{
			
			if (livePlayer == 0)
			{
				DeadPlayer();
			}
			else
			{
				gameObject.transform.position = newposition;
				botGenerator.gameObject.GetComponent<GenerateBot>().playerDeath = true;
				triggerSound.gameObject.GetComponent<MakeSound>().playerDead = true;
				livePlayer = livePlayer - 1;
			}
		}
		else
		{
			botGenerator.gameObject.GetComponent<GenerateBot>().botDestroyed = true;
			Destroy(other.gameObject);
			score = score + 200;
		}
	}
}
