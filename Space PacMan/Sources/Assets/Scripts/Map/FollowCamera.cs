﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
	public GameObject player;

	/*
	** Déplace la caméra en fonction de la position de player en x et z
	*/
	void Update ()
	{
		Vector3 newposition = new Vector3 (player.transform.position.x, gameObject.transform.position.y, player.transform.position.z);
		gameObject.transform.position = newposition;
	}
}
