﻿using UnityEngine;
using System.Collections;

public class MakeSound : MonoBehaviour
{
	public AudioClip sound;
	public bool playerDead = false;
	int incrementBots;
	bool soundPlay = false;
	public GameObject botGenerator;

	/*
	** Check les conditions de lecture du son
	*/
	void Update ()
	{
		if (incrementBots == 0 && audio.volume > 0f)
		{
			audio.volume = audio.volume - 0.01f;
		}
		if (audio.volume <= 0f)
		{
			soundPlay = false;
		}
		if (playerDead == true)
		{
			audio.Stop();
			incrementBots = 0;
			playerDead = false;
			soundPlay = false;
		}
		if (incrementBots == 1 && soundPlay == false)
		{
			audio.volume = 0.25f;
			audio.PlayOneShot (sound);
			soundPlay = true;
		}
		if (botGenerator.gameObject.GetComponent<GenerateBot> ().botDestroyed == true)
		{
			incrementBots = incrementBots - 1;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			incrementBots = incrementBots + 1;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			incrementBots = incrementBots - 1;
		}
	}
}
