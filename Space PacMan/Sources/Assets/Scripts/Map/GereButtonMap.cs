﻿using UnityEngine;
using System.Collections;

public class GereButtonMap : MonoBehaviour
{
	public AudioClip sound;
	public GameObject menuInPlay;

	/*
	** Permet de retourner en jeu
	*/
	public void ResumeGame()
	{
		audio.PlayOneShot (sound);
		menuInPlay.SetActive(false);
		Time.timeScale = 1;
	}

	/*
	** Permet de retourner au menu
	*/
	public void ReturnMenu()
	{
		audio.PlayOneShot (sound);
		Time.timeScale = 1;
		Application.LoadLevel("Menu");
	}

	/*
	** Permet de relancer une partie
	*/
	public void Retry()
	{
		audio.PlayOneShot (sound);
		Application.LoadLevel ("Map");
	}
}
