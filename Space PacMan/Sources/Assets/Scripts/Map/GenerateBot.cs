﻿using UnityEngine;
using System.Collections;

public class GenerateBot : MonoBehaviour
{
	public GameObject Bot;
	public Vector3 PositionSpawn;
	public bool botDestroyed = false;
	public bool playerDeath = false;
	public Transform Player;

	/*
	** Vérifie si un bot à été détruit ou si un joueur est mort
	*/
	void Update ()
	{
		if (botDestroyed == true)
		{
			CreateBot();
		}
		if (playerDeath == true)
		{
			ReinitBot();
		}
	}

	/*
	** Créer un nouveau bot avec des valeurs par défaut
	*/
	void CreateBot()
	{
		GameObject newBot;
		newBot = Instantiate(Bot, PositionSpawn, gameObject.transform.rotation) as GameObject;
		newBot.gameObject.GetComponent<PathFindingIA>().WaitTimer = 5f;
		newBot.gameObject.GetComponent<PathFindingIA>().Player = Player;
		newBot.gameObject.tag = "Enemy";
		botDestroyed = false;
	}

	/*
	**	Supprime tout les robots de la bots et recréer 4 robots 
	*/
	void ReinitBot()
	{
		GameObject[] bots;
		int i = 0;

		bots = GameObject.FindGameObjectsWithTag ("Enemy");
		while (i != 4)
		{
			Destroy(bots[i]);
			i++;
		}
		i = 0;
		while (i != 4)
		{
			GameObject newBot;
			newBot = Instantiate(Bot, PositionSpawn, gameObject.transform.rotation) as GameObject;
			newBot.gameObject.GetComponent<PathFindingIA>().WaitTimer = (i * 5f);
			newBot.gameObject.GetComponent<PathFindingIA>().Player = Player;
			newBot.gameObject.tag = "Enemy";
			i = i + 1;
		}
		playerDeath = false;
	}
}
