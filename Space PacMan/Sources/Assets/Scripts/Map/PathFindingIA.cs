﻿using UnityEngine;
using System.Collections;

public class PathFindingIA : MonoBehaviour
{
	public float WaitTimer;
	public Transform Player;
	public Vector3 SpawnPoint;
	
	/*
	** Update is called once per frame
	*/
	void Update ()
	{
		if (WaitTimer > 0)
		{
			WaitTimer -= Time.deltaTime;
		}
		else 
		{
			startIA();
		}
	}
	
	void startIA()
	{
		GetComponent<NavMeshAgent> ().destination = Player.position;
	}
}