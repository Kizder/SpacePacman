﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GereButton : MonoBehaviour
{
	public string levelSuivant;
	public AudioClip sound;
	public GameObject menuCredits;
	public GameObject menuBase;

	void Start ()
	{

	}
	
	void Update ()
	{

	}

	public void QuitGame()
	{
		audio.PlayOneShot (sound);
		Application.Quit();
	}

	public void changeLevel()
	{
		audio.PlayOneShot (sound);
		Application.LoadLevel (levelSuivant);
	}

	public void creditsMenu()
	{
		audio.PlayOneShot (sound);
		menuBase.gameObject.SetActive (false);
		menuCredits.gameObject.SetActive (true);
	}

	public void returnMenu()
	{
		audio.PlayOneShot (sound);
		menuBase.gameObject.SetActive (true);
		menuCredits.gameObject.SetActive (false);
	}
}
